#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

echo -e "${CYAN}Enter the FQDN for the new site: ${NC}"
read fqdn

echo -e "${CYAN}Enter the title line for the new site: ${NC}"
read title

echo -e "${CYAN}Enter the subtitle line for the new site: ${NC}"
read subtitle

fileext=".conf"
file="/etc/apache2/sites-available/$fqdn$fileext"

echo -e "${CYAN}Which folder should the website folder be under?${NC}"

declare -a dirs
i=1
for d in /var/www/*/
do
    dirs[i++]="${d%/}"
done
for((i=1;i<=${#dirs[@]};i++))
do
    echo $i "${dirs[i]}"
done
echo -n "> "
read i
folder=${dirs[$i]}
echo -e "${CYAN}Creating root folder $folder/$fqdn${NC}"

mkdir "$folder/$fqdn"

echo -e "${GREEN}Done.${NC}"
echo -e "${CYAN}Creating index file at $folder/$fqdn/index.html${NC}"

cat > "$folder/$fqdn/index.html" <<EOF
<!doctype html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>$title</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <style>
            html, body {background-color: #fff; color: #636b6f; font-family: 'Raleway', sans-serif; font-weight: 100; height: 100vh; margin: 0;}
            .full-height {height: 100vh;}
            .flex-center {align-items: center; display: flex; justify-content: center;}
            .position-ref {position: relative;}
            .top-right {position: absolute; right: 10px; top: 18px;}
            .content {text-align: center;}
            .title {font-size: 84px;}
            .links > a {color: #636b6f;padding: 0 25px;font-size: 12px;font-weight: 600;letter-spacing: .1rem;text-decoration: none;text-transform: uppercase;}
            .m-b-md {margin-bottom: 30px;}
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">$title</div>
                <div class="m-b-md">$subtitle</div>
            </div>
        </div>
    </body>
</html>

EOF
chown www-data:www-data "$folder/$fqdn"

echo -e "${GREEN}Done.${NC}"
echo -e "${CYAN}Writing config file $file${NC}"

cat > "$file" <<EOF
<VirtualHost *:80>
    DocumentRoot "$folder/$fqdn"
    ServerName $fqdn
    <Directory "$folder/$fqdn">
        AllowOverride All
        allow from all
        Options +Indexes
    </Directory>
</VirtualHost>
EOF

echo -e "${GREEN}Done.${NC}"
echo -e "${CYAN}Calling Apache2 to enable $fqdn${NC}"

a2ensite "$fqdn.conf"
service apache2 reload

echo -e "${GREEN}Done.${NC}"
echo -e "${CYAN}Calling CertBot to generate a SSL certificate for $fqdn${NC}"

#certbot --authenticator standalone --installer apache --domain "$fqdn" --redirect --hsts --pre-hook "service apache2 stop" --post-hook "service apache2 start" 
certbot --apache --domain "$fqdn" --redirect --hsts

echo -e "${GREEN}Done.${NC}"

