#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

echo -e "${CYAN}Enter the FQDN for the site: ${NC}"
read fqdn

fileext=".conf"
file="/etc/apache2/sites-available/$fqdn$fileext"
sslfile="/etc/apache2/sites-available/$fqdn-le-ssl$fileext"

echo -e "${CYAN}Which folder should the website folder be under?${NC}"

declare -a dirs
i=1
for d in /var/www/*/
do
    dirs[i++]="${d%/}"
done
for((i=1;i<=${#dirs[@]};i++))
do
    echo $i "${dirs[i]}"
done
echo -n "> "
read i
folder=${dirs[$i]}

echo -e "${CYAN}Calling Apache2 to disable $fqdn${NC}"
a2dissite "$fqdn.conf"
unlink "$file"
a2dissite "$fqdn-le-ssl.conf"
unlink "$sslfile"

echo -e "${GREEN}Done.${NC}"
echo -e "${CYAN}Calling CertBot to delete SSL certificate for $fqdn${NC}"

certbot delete --cert-name "$fqdn" 

echo -e "${CYAN}Deleting root folder $folder/$fqdn${NC}"
rm -rf "$folder/$fqdn"

echo -e "${GREEN}Done.${NC}"
echo -e "${CYAN}Restarting Apache2.${NC}"
service apache2 reload

echo -e "${GREEN}Done.${NC}"
